package com.pixelcreon.bangladesh;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BarishalActivity extends FragmentActivity {


    ViewPager viewpager;
    SwipeAdapterBarishal adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barishal);
        adapter = new SwipeAdapterBarishal(getSupportFragmentManager(),this);

        viewpager = (ViewPager)findViewById(R.id.viewpager);
        viewpager.setAdapter(adapter);
    }
}
