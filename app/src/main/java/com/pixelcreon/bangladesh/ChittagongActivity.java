package com.pixelcreon.bangladesh;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

public class ChittagongActivity extends FragmentActivity {

    ViewPager viewpager;
    SwipeAdapterChittagong adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chittagong);
        adapter = new SwipeAdapterChittagong(getSupportFragmentManager(),this);

        viewpager = (ViewPager)findViewById(R.id.viewpager);
        viewpager.setAdapter(adapter);
    }
}
