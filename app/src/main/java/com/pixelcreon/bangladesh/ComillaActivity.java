package com.pixelcreon.bangladesh;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ComillaActivity extends FragmentActivity {


    ViewPager viewpager;
    SwipeAdapterComilla adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comilla);
        adapter = new SwipeAdapterComilla(getSupportFragmentManager(),this);

        viewpager = (ViewPager)findViewById(R.id.viewpager);
        viewpager.setAdapter(adapter);
    }
}
