package com.pixelcreon.bangladesh;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;

public class DhakaActivity extends FragmentActivity {

    ViewPager viewpager;
    SwipeAdapter adapter;
    TabLayout tlDhaka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dhaka);
        adapter = new SwipeAdapter(getSupportFragmentManager(),this);

        viewpager = (ViewPager) findViewById(R.id.viewpager);
        tlDhaka = (TabLayout)findViewById(R.id.tab_layout_dhaka);

        viewpager.setAdapter(adapter);
        tlDhaka.setupWithViewPager(viewpager);

        disableTabStrip();

    }

    public void disableTabStrip() {
        LinearLayout tabStrip = (LinearLayout) tlDhaka.getChildAt(0);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }
}
