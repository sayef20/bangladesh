package com.pixelcreon.bangladesh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button dhaka,chittagong,hilltrack, khulna, rajshahi, sylhet, barishal, rangpur, mymensingh, comilla;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dhaka = (Button) findViewById(R.id.dhaka);
        chittagong = (Button) findViewById(R.id.chittagong);
        hilltrack = (Button) findViewById(R.id.hilltrack);
        khulna = (Button) findViewById(R.id.khulna);
        sylhet = (Button) findViewById(R.id.sylhet);
        rajshahi = (Button) findViewById(R.id.rajshahi);
        barishal = (Button) findViewById(R.id.barishal);
        rangpur = (Button) findViewById(R.id.rangpur);
        mymensingh = (Button) findViewById(R.id.mymensingh);
        comilla = (Button) findViewById(R.id.comilla);

        dhaka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_dhaka = new Intent(MainActivity.this, DhakaActivity.class);
                startActivity(intent_dhaka);
            }
        });
        chittagong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_chittagong = new Intent(MainActivity.this, ChittagongActivity.class);
                startActivity(intent_chittagong);
            }
        });

        hilltrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_hilltrack = new Intent(MainActivity.this, HilltrackActivity.class);
                startActivity(intent_hilltrack);
            }
        });

        khulna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_khulna = new Intent(MainActivity.this, KhulnaActivity.class);
                startActivity(intent_khulna);
            }
        });

        rajshahi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_rajshahi = new Intent(MainActivity.this, RajshahiActivity.class);
                startActivity(intent_rajshahi);
            }
        });

        sylhet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_sylhet = new Intent(MainActivity.this, SylhetActivity.class);
                startActivity(intent_sylhet);
            }
        });

        barishal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_barishal = new Intent(MainActivity.this, BarishalActivity.class);
                startActivity(intent_barishal);
            }
        });

        rangpur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_rangpur = new Intent(MainActivity.this, RangpurActivity.class);
                startActivity(intent_rangpur);
            }
        });

        mymensingh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_mymensingh = new Intent(MainActivity.this, MymensinghActivity.class);
                startActivity(intent_mymensingh);
            }
        });

        comilla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent_comilla = new Intent(MainActivity.this, ComillaActivity.class);
                startActivity(intent_comilla);
            }
        });
    }
}
