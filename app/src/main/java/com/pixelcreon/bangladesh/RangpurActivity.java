package com.pixelcreon.bangladesh;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

public class RangpurActivity extends FragmentActivity {


    ViewPager viewpager;
    SwipeAdapterRangpur adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rajshahi);
        adapter = new SwipeAdapterRangpur(getSupportFragmentManager(),this);

        viewpager = (ViewPager)findViewById(R.id.viewpager);
        viewpager.setAdapter(adapter);
    }
}
