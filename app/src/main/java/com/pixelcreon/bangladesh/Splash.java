package com.pixelcreon.bangladesh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class Splash extends AppCompatActivity {

    private ImageView splashimage;
    private TextView splashtxttop, splashtxtbottom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashimage = (ImageView) findViewById(R.id.splashlogo);
        splashtxttop = (TextView) findViewById(R.id.splashtexttop);
        splashtxtbottom = (TextView) findViewById(R.id.splashtextbottom);

        final Intent intent = new Intent(this,MainActivity.class);

        Animation myanimation = AnimationUtils.loadAnimation(this, R.anim.splash_transition);
        splashimage.startAnimation(myanimation);
        splashtxttop.startAnimation(myanimation);
        splashtxtbottom.startAnimation(myanimation);

        Thread timer = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(intent);
                    finish();
                }
            }
        };

        timer.start();
    }
}
