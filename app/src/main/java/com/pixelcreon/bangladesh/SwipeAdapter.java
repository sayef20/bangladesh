package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapter extends FragmentPagerAdapter{

    String[] places;
    String[] placesDescription;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapter(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        places = resources.getStringArray(R.array.places);
        placesDescription =resources.getStringArray(R.array.place_description);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getDhakaImages(position));
        bundle.putString(DescriptionKey, placesDescription[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getDhakaImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.lalbag;
                break;
            case 1:
                id = R.drawable.ahsanmonjil;
                break;
            case 2:
                id = R.drawable.hatirjheel;
                break;
            case 3:
                id = R.drawable.sritishoudha;
                break;
        }
        return id;

    }
    //Page Title Commented because TabLayout consists of circles & is not expected to show Titles
    //If you want the titles back, remove these 2 TabLayout properties in XML Layout : background & tabGravity = center
    // and enable below code - Tahmid (29-8-2017)

//    @Override
//    public CharSequence getPageTitle(int position) {
//        return places[position];
//
//    }

    public int getCount() {
        return places.length;
    }

}
