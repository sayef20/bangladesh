package com.pixelcreon.bangladesh;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterBarishal extends FragmentPagerAdapter{


    String[] placesBarishal;
    String[] placesDescriptionBarishal;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterBarishal(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesBarishal = resources.getStringArray(R.array.places_barishal);
        placesDescriptionBarishal =resources.getStringArray(R.array.place_description_barishal);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getBarishalImages(position));
        bundle.putString(DescriptionKey, placesDescriptionBarishal[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getBarishalImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.kuakata;
                break;
            case 1:
                id = R.drawable.guavamarket;
                break;
            case 2:
                id = R.drawable.durgashagor;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesBarishal[position];

    }

    public int getCount() {
        return placesBarishal.length;
    }



}
