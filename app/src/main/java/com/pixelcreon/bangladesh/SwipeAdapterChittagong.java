package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterChittagong extends FragmentPagerAdapter{

    String[] placesChittagong;
    String[] placesDescriptionChittagong;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterChittagong(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesChittagong = resources.getStringArray(R.array.places_chittagong);
        placesDescriptionChittagong =resources.getStringArray(R.array.place_description_chittagong);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getChittagongImages(position));
        bundle.putString(DescriptionKey, placesDescriptionChittagong[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getChittagongImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.inani;
                break;
            case 1:
                id = R.drawable.potenga;
                break;
            case 2:
                id = R.drawable.saintmartin;
                break;
            case 3:
                id = R.drawable.kutubdia;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesChittagong[position];

    }

    public int getCount() {
        return placesChittagong.length;
    }

}
