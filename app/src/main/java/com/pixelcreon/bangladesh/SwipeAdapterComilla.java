package com.pixelcreon.bangladesh;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterComilla extends FragmentPagerAdapter {


    String[] placesComilla;
    String[] placesDescriptionComilla;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterComilla(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesComilla = resources.getStringArray(R.array.places_comilla);
        placesDescriptionComilla =resources.getStringArray(R.array.place_description_comilla);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getChittagongImages(position));
        bundle.putString(DescriptionKey, placesDescriptionComilla[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getChittagongImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.lalmai;
                break;
            case 1:
                id = R.drawable.shalbonbihar;
                break;
            case 2:
                id = R.drawable.warcemetry;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesComilla[position];

    }

    public int getCount() {
        return placesComilla.length;
    }
}
