package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterHilltrack extends FragmentPagerAdapter {

    String[] placesHilltrack;
    String[] placesDescriptionHilltrack;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterHilltrack(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesHilltrack = resources.getStringArray(R.array.places_hilltrack);
        placesDescriptionHilltrack =resources.getStringArray(R.array.place_description_hilltrack);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getHilltrackImages(position));
        bundle.putString(DescriptionKey, placesDescriptionHilltrack[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getHilltrackImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.rangamati;
                break;
            case 1:
                id = R.drawable.nilgiri;
                break;
            case 2:
                id = R.drawable.bogalake;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesHilltrack[position];

    }

    public int getCount() {
        return placesHilltrack.length;
    }
}
