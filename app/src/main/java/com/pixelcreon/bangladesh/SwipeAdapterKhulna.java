package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterKhulna extends FragmentPagerAdapter {

    String[] placesKhulna;
    String[] placesDescriptionKhulna;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterKhulna(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesKhulna = resources.getStringArray(R.array.places_khulna);
        placesDescriptionKhulna =resources.getStringArray(R.array.place_description_khulna);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getChittagongImages(position));
        bundle.putString(DescriptionKey, placesDescriptionKhulna[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getChittagongImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.koromjol;
                break;
            case 1:
                id = R.drawable.shatgombuz;
                break;
            case 2:
                id = R.drawable.khanjahanalimazar;
                break;
            case 3:
                id = R.drawable.lalonshahmazar;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesKhulna[position];

    }

    public int getCount() {
        return placesKhulna.length;
    }
}
