package com.pixelcreon.bangladesh;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterMymensingh extends FragmentPagerAdapter{


    String[] placesMymensingh;
    String[] placesDescriptionMymensingh;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterMymensingh(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesMymensingh = resources.getStringArray(R.array.places_mymensingh);
        placesDescriptionMymensingh =resources.getStringArray(R.array.place_description_mymensingh);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getMymensinghImages(position));
        bundle.putString(DescriptionKey, placesDescriptionMymensingh[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getMymensinghImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.chinamatirlake;
                break;
            case 1:
                id = R.drawable.modhutila;
                break;
            case 2:
                id = R.drawable.joynulabedin;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesMymensingh[position];

    }

    public int getCount() {
        return placesMymensingh.length;
    }
}
