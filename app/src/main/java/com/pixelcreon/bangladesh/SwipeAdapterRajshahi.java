package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class SwipeAdapterRajshahi extends FragmentPagerAdapter {

    String[] placesRajshahi;
    String[] placesDescriptionRajshahi;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterRajshahi(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesRajshahi = resources.getStringArray(R.array.places_rajshahi);
        placesDescriptionRajshahi =resources.getStringArray(R.array.place_description_rajshahi);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getChittagongImages(position));
        bundle.putString(DescriptionKey, placesDescriptionRajshahi[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getChittagongImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.chotoshonamoszid;
                break;
            case 1:
                id = R.drawable.mohasthangar;
                break;
            case 2:
                id = R.drawable.chalanbil;
                break;
            case 3:
                id = R.drawable.puthiatemple;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesRajshahi[position];

    }

    public int getCount() {
        return placesRajshahi.length;
    }
}
