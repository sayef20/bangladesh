package com.pixelcreon.bangladesh;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterRangpur extends FragmentPagerAdapter{


    String[] placesRangpur;
    String[] placesDescriptionRangpur;


    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterRangpur(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesRangpur = resources.getStringArray(R.array.places_rangpur);
        placesDescriptionRangpur =resources.getStringArray(R.array.place_description_rangpur);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getRangpurImages(position));
        bundle.putString(DescriptionKey, placesDescriptionRangpur[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }

    private int getRangpurImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.ramsagar;
                break;
            case 1:
                id = R.drawable.tajhatpalace;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesRangpur[position];

    }

    public int getCount() {
        return placesRangpur.length;
    }
}
