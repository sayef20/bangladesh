package com.pixelcreon.bangladesh;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SwipeAdapterSylhet extends FragmentPagerAdapter {

    String[] placesSylhet;
    String[] placesDescriptionSylhet;

    public static final String ImageIDKey = "imagekey";
    public static final String DescriptionKey = "descriptionkey";


    public SwipeAdapterSylhet(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();

        placesSylhet = resources.getStringArray(R.array.places_sylhet);
        placesDescriptionSylhet =resources.getStringArray(R.array.place_description_sylhet);

    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();

        bundle.putInt(ImageIDKey,getSylhetImages(position));
        bundle.putString(DescriptionKey, placesDescriptionSylhet[position]);
        PageFragment pageFragment = new PageFragment();
        pageFragment.setArguments(bundle);

        return pageFragment;
    }


    private int getSylhetImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.jaflong;
                break;
            case 1:
                id = R.drawable.lawachara;
                break;
            case 2:
                id = R.drawable.tanguarhaor;
                break;
            case 3:
                id = R.drawable.ratargul;
                break;
            case 4:
                id = R.drawable.madhabkunda;
                break;
        }
        return id;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        return placesSylhet[position];

    }

    public int getCount() {
        return placesSylhet.length;
    }
}
